# repos

repo_name.db - files
repo_name.db.tar.gz
repo_name.files - files
repo_name.files.tar.gz

package_name-0.0-0-x86_64.pkg.tar.zst
package_name-0.0-0-x86_64.pkg.tar.zst.sig


sublime-text-4126-1/000755 000000 000000 00000000000 14160237253 014211 5ustar00rootroot000000 000000 sublime-text-4126-1/desc000644 000000 000000 00000002356 14160237253 015060 0ustar00rootroot000000 000000 %FILENAME%
sublime-text-4126-1-x86_64.pkg.tar.xz

%NAME%
sublime-text

%VERSION%
4126-1

%DESC%
Sublime Text is a sophisticated text editor for code, markup and prose

%CSIZE%
17189696

%ISIZE%
50612948

%MD5SUM%
85ef691cd4ddb032537f46ed0e6fc942

%SHA256SUM%
71140a4ebf09b4069ce150e380b95355e953627e67d4322fb80528492a1a3a95

%PGPSIG%
iQIcBAABCAAGBQJhwT0jAAoJEPV9T1m9PfRUtDoQAJnxoEOkYgtNR9Xet70usOXRLx/0+QuI/rG9doDEElyeI4UCdoixTa65/Aq+gKlE2VLDkb05Gjen3Bv1ZRwDTvkX7xdHoddj09O0iYZvR1reLW1DopDiADwL93xRJhHMvIWQyUBxniCuEVrKgHSV7Fx5yDIKs6MAxtCIh1TjCWLFnx8FEc+JewGSwOB05UsTHAZXv+Qwmrc0jf8WOdFVRNvTN/6Ja+6RK1IFMAibgnKx9VsRXjqZqySmcymEW9RNqK0g5+Pi5FGKU/fVnhw6CmEpXQ76EgxdXMDOPWJqhgD9EquVEvTqQhksEomgLjKUuh70M66YUtigqraFXJw6O/4IVKm2xry1KeYxjKgECZU2dd270JojUgwLFVtjxOpAtwG/641t9NdRShaq5UhyGg43Y26Q/AtL9SWsEQ3LBEC6QNQPY3DogSP/rF+bePy9RQTlTGYYbvNmjpOHqX3jLsUo+K7hRIUUrlO7Vi3ImM6C8FWqbdcHeT2ZMzeTkFqScbZT5/+qP4WqSuiGV6tWFY4HYTjlNs/vg7tr45vaKp7okVHq2lWBGbmw/jHGCtb+XGYyu5GKSmy435oWxFEf5O+OUmL5hrc1lOfYCh+EEcA4v3J2FDp4eKEhPIETRLZitqd0EAYmAFSjgV6thpEmresQ3312rnPFwUOoZoms8fEl

%URL%
https://www.sublimetext.com

%LICENSE%
custom

%ARCH%
x86_64

%BUILDDATE%
1640054019

%PACKAGER%
Sublime HQ Packager

%CONFLICTS%
sublime-text-dev
sublime-text-nightly

%DEPENDS%
gtk3
libcurl.so